# Working with Legal Doc HTML (LD)

**Legal Doc** is Libryo's proprietary standard for structuring legal documents in HTML. This means, when you have a legal document in any markup format i.e. HTML, XML or PDF, you can write rules that will convert it into a standard format, Legal Doc, that can be parsed into Akoma Ntoso(AkN) format. In this document, we focus on HTML format.

> Read more about Akoma Ntoso [here](https://en.wikipedia.org/wiki/Akoma_Ntoso) and [here](http://www.akomantoso.org/?page_id=25) and Legal Doc [here](docs/legal-doc-html.md).

## General Development Environment Setup

Typically, [VS Code](https://code.visualstudio.com/) is the common text editor that's used because of it's debugging features and ability to add extra extensions.

### Strategies for making conversions:

#### Using config

// TODO: Evans R.

#### Using the source

This document explains this strategy. You will need to have the [legal-doc-html](https://gitlab.com/libryo/legal-doc-html) repository cloned. Then you can create a tranformation file with your title i.e. `Bangladesh.ts` in the [src/Transformer/OptimusTransformer/Sources/](https://gitlab.com/libryo/legal-doc-html/tree/master/src/Transformer/OptimusTransformer/Sources) for a Bangladesh source transformation file.

There are 2 strategies to get your conversion code to run:

1. Make a `.js` file in the project folder and run it with `Node JS`. You will need to run the `watch` npm script command first to get your source transformation file i.e. `Bangladesh.ts` file to be compiled from a `.ts` to a `.js` file in the `build` directory of the project source. It's a one time run command so do it in one terminal and run the `node bangladesh-transform.js` in another.

2. Create a `.test` file and use `Jest` to run it as a test. This is a better approach as it allows you to be able to debug your code i.e. make breakpoints in the code.

   > Read more about setting up [jest-runner](https://marketplace.visualstudio.com/items?itemName=firsttris.vscode-jest-runner) on VS code here.

Either way, the end goal is to be able to:

1. Get HTML string as input.
2. Run it through a source transformation.
3. Get legal doc html string as output.
4. Convert the legal doc html string to AkN.

## Step-by-step setup and conversion

This is a section of the Australian Act [Administrative Appeals Tribunal Act 1975](https://www.legislation.gov.au/Details/C2018C00172).

```html
<div class="WordSection3">
    <p class="LongT"><span lang="EN-AU">An Act to establish an Administrative Appeals Tribunal</span></p>
    <p class="ActHead2"><a name="_Toc514678017"><span class="CharPartNo"><span lang="EN-AU">PartI</span></span><span
                lang="EN-AU">—<span class="CharPartText">Preliminary</span></span></a></p>
    <p class="MsoHeader"><span class="CharDivNo"><span lang="EN-AU"></span></span><span class="CharDivText"><span
                lang="EN-AU"></span></span>
    </p>
    <p class="ActHead5"><a name="_Toc514678018"><span class="CharSectno"><span lang="EN-AU">1</span></span><span
                lang="EN-AU"> Short title</span></a></p>
    <p class="subsection"><span lang="EN-AU"> This Act may be cited as the <i>Administrative Appeals Tribunal Act
                1975</i>.</span></p>
    <p class="ActHead5"><a name="_Toc514678019"><span class="CharSectno"><span lang="EN-AU">2</span></span><span
                lang="EN-AU"> Commencement</span></a></p>
    <p class="subsection"><span lang="EN-AU"> This Act shall come into operation on a day to be fixed by
            Proclamation.</span></p>
    <p class="ActHead5"><a name="_Toc514678020"><span class="CharSectno"><span lang="EN-AU">2A</span></span><span
                lang="EN-AU"> Tribunal’s objective</span></a></p>
    <p class="subsection"><span lang="EN-AU"> In carrying out its functions, the Tribunal must pursue the objective of
            providing a mechanism of review that:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) is accessible; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) is fair, just, economical, informal and quick; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) is proportionate to the importance and complexity of the matter;
            and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) promotes public trust and confidence in the decision‑making of the
            Tribunal.</span></p>
    <p class="ActHead5"><a name="_Toc514678021"><span class="CharSectno"><span lang="EN-AU">3</span></span><span
                lang="EN-AU"> Interpretation</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) In this Act, unless the contrary intention appears:</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">adduce</span></i></b><span lang="EN-AU"> includes give.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">agency party</span></i></b><span lang="EN-AU"> means a party who
            is:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) the Secretary of a Department; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) the Chief Executive Medicare; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) the Chief Executive Centrelink; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) the Child Support Registrar.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">alternative dispute resolution processes</span></i></b><span
            lang="EN-AU"> means procedures and services for the resolution of disputes, and includes:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) conferencing; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) mediation; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) neutral evaluation; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) case appraisal; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) conciliation; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (f) procedures or services specified in the regulations;</span></p>
    <p class="subsection2"><span lang="EN-AU">but does not include:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (g) arbitration; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (h) court procedures or services.</span></p>
    <p class="subsection2"><span lang="EN-AU">Paragraphs(b) to (f) of this definition do not limit paragraph(a) of this
            definition.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">ASIO affiliate</span></i></b><span lang="EN-AU"> has the same meaning
            as in the <i>Australian Security Intelligence Organisation Act 1979</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">ASIO employee</span></i></b><span lang="EN-AU"> has the same meaning
            as in the <i>Australian Security Intelligence Organisation Act 1979</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">ASIO Minister</span></i></b><span lang="EN-AU"> means the Minister
            administering the <i>Australian Security Intelligence Organisation Act 1979</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">authorised member</span></i></b><span lang="EN-AU"> means a member
            who has been authorised by the President under section59A for the purposes of the provision in which the
            expression occurs.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">authorised officer</span></i></b><span lang="EN-AU"> means an officer
            of the Tribunal who has been authorised by the President under section59B for the purposes of the provision
            in which the expression occurs.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">authority of Norfolk Island</span></i></b><span lang="EN-AU"> means
            an authority, tribunal or other body, whether incorporated or not, that is established by a Norfolk Island
            enactment.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">authority of the Commonwealth</span></i></b><span lang="EN-AU"> means
            an authority, tribunal or other body, whether incorporated or not, that is established by an
            enactment.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Chief Executive Centrelink</span></i></b><span lang="EN-AU"> has the
            same meaning as in the <i>Human Services (Centrelink) Act 1997</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Chief Executive Medicare</span></i></b><span lang="EN-AU"> has the
            same meaning as in the <i>Human Services (Medicare) Act 1973</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">child support first review</span></i></b><span lang="EN-AU"> means a
            proceeding that is or would be a proceeding in the Social Services and Child Support Division on application
            for AAT first review within the meaning of the <i>Child Support (Registration and Collection) Act
                1988</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Commonwealth agency</span></i></b><span lang="EN-AU"> means a
            Minister or an authority of the Commonwealth.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">CSC</span></i></b><span lang="EN-AU"> (short for Commonwealth
            Superannuation Corporation) has the same meaning as in the <i>Governance of Australian Government
                Superannuation Schemes Act 2011</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Deputy President</span></i></b><span lang="EN-AU"> means a member
            appointed as a Deputy President of the Tribunal.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">enactment</span></i></b><span lang="EN-AU"> means:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) an Act;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) an Ordinance of a Territory other than the Northern Territory, the
            Australian Capital Territory or Norfolk Island; or</span></p>
    <p class="paragraph" style="page-break-after:avoid"><span lang="EN-AU"> (c) an instrument (including rules,
            regulations or by‑laws) made under an Act or under such an Ordinance;</span></p>
    <p class="subsection2"><span lang="EN-AU">and includes an enactment as amended by another enactment.</span></p>
    <p class="notetext"><span lang="EN-AU">Note: See also subsection25(8) (Norfolk Island enactments).</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">engage in conduct</span></i></b><span lang="EN-AU"> has the same
            meaning as in the <i>Criminal Code</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">full‑time member</span></i></b><span lang="EN-AU"> means a member who
            is appointed as a full‑time member.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">immigration advisory service</span></i></b><span lang="EN-AU"> means
            a body that provides services in relation to the seeking by non‑citizens (within the meaning of the
            <i>Migration Act 1958</i>) of permission to enter or remain in Australia.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Judge</span></i></b><span lang="EN-AU"> means:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) a Judge of a court created by the Parliament; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) a person who has the same designation and status as a Judge of a court
            created by the Parliament.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">member</span></i></b><span lang="EN-AU"> means:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) the President; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) a Deputy President; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) a senior member; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) any other member (of any level referred to in subsection6(3)).</span>
    </p>
    <p class="Definition"><b><i><span lang="EN-AU">non‑presidential member</span></i></b><span lang="EN-AU"> means a
            member other than a presidential member.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Norfolk Island enactment</span></i></b><span lang="EN-AU">
            means:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) an enactment (within the meaning of the <i>Norfolk Island Act
                1979</i>); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) an instrument (including rules, regulations or by‑laws) made under such
            an enactment;</span></p>
    <p class="subsection2"><span lang="EN-AU">and includes a Norfolk Island enactment as amended by another Norfolk
            Island enactment.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">officer of the Tribunal</span></i></b><span lang="EN-AU">
            means:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) the Registrar; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) a person appointed as an officer of the Tribunal under
            section24PA.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">part‑time member</span></i></b><span lang="EN-AU"> means a member who
            is appointed as a part‑time member.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">person who made the decision </span></i></b><span lang="EN-AU">has a
            meaning affected by:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) if a review of the decision is or would be an AAT first review within
            the meaning of the <i>A New Tax System (Family Assistance) (Administration) Act 1999</i>—section111B of that
            Act; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) if a review of the decision is or would be an AAT first review within
            the meaning of the <i>Paid Parental Leave Act 2010</i>—section224A of that Act; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) if a review of the decision is or would be an AAT first review within
            the meaning of the <i>Social Security (Administration) Act 1999</i>—section142A of that Act; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) if a review of the decision is or would be an AAT first review within
            the meaning of the <i>Student Assistance Act 1973</i>—section311A of that Act.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">President</span></i></b><span lang="EN-AU"> means the President of
            the Tribunal.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">presidential member</span></i></b><span lang="EN-AU"> means the
            President or a Deputy President.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">proceeding</span></i></b><span lang="EN-AU">, in relation to the
            Tribunal, includes:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) an application to the Tribunal for review of a decision; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) an application to the Tribunal under subsection28(1AC); and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) an application to the Tribunal for review of a taxing of costs;
            and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) an application to the Tribunal for a costs certificate under section10A
            of the <i>Federal Proceedings (Costs) Act 1981</i>; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) an application to the Tribunal under subsection62(2) of the <i>Freedom
                of Information Act 1982</i>; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (f) any other application to the Tribunal under this Act or any other Act;
            and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (g) any matter referred to the Tribunal for inquiry and/or review under
            this Act or any other Act; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (h) an incidental application to the Tribunal made in the course of, or in
            connection with, an application or proposed application, or a matter, referred to in a preceding
            paragraph.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Registrar</span></i></b><span lang="EN-AU"> means the Registrar of
            the Tribunal.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">second review</span></i></b><span lang="EN-AU">: a review by the
            Tribunal of a decision is or would be a <b><i>second review</i></b> if another enactment:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) authorises an application to be made for review of the decision;
            and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) designates the review as an <b><i>AAT second review</i></b>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">security assessment</span></i></b><span lang="EN-AU"> or
            <b><i>assessment</i></b> has the same meaning as in the <i>Australian Security Intelligence Organisation Act
                1979</i>.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">senior member</span></i></b><span lang="EN-AU"> means a senior member
            of the Tribunal (of either level referred to in subsection6(3)).</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">State</span></i></b><span lang="EN-AU"> includes the Northern
            Territory and the Australian Capital Territory.</span></p>
    <p class="Definition"><b><i><span lang="EN-AU">Tribunal</span></i></b><span lang="EN-AU">:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) means the Administrative Appeals Tribunal established by this Act;
            and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) in relation to a proceeding, means the Administrative Appeals Tribunal
            so established as constituted for the purposes of the proceeding; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) includes a member, or an officer of the Tribunal, exercising powers of
            the Tribunal.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) Where a board, committee or other unincorporated body constituted by 2
            or more persons is empowered by an enactment to make decisions, this Act applies as if that board, committee
            or other body were a person empowered to make those decisions.</span></p>
    <p class="subsection" style="page-break-after:avoid"><span lang="EN-AU"> (3) Unless the contrary intention appears,
            a reference in this Act to a decision includes a reference to:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) making, suspending, revoking or refusing to make an order or
            determination;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) giving, suspending, revoking or refusing to give a certificate,
            direction, approval, consent or permission;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) issuing, suspending, revoking or refusing to issue a licence, authority
            or other instrument;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) imposing a condition or restriction;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) making a declaration, demand or requirement;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (f) retaining, or refusing to deliver up, an article; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (g) doing or refusing to do any other act or thing.</span></p>
    <p class="ActHead5"><a name="_Toc514678022"><span class="CharSectno"><span lang="EN-AU">4</span></span><span
                lang="EN-AU"> Extension to external Territories</span></a></p>
    <p class="subsection"><span lang="EN-AU"> This Act extends to every external Territory.</span></p>
    <p class="ActHead2" style="page-break-before:always"><a name="_Toc514678023"><span class="CharPartNo"><span
                    lang="EN-AU">PartII</span></span><span lang="EN-AU">—<span class="CharPartText">Establishment of the
                    Administrative Appeals Tribunal</span></span></a></p>
    <p class="ActHead3"><a name="_Toc514678024"><span class="CharDivNo"><span lang="EN-AU">Division1</span></span><span
                lang="EN-AU">—<span class="CharDivText">Establishment of Tribunal</span></span></a></p>
    <p class="ActHead5"><a name="_Toc514678025"><span class="CharSectno"><span lang="EN-AU">5</span></span><span
                lang="EN-AU"> Establishment of Tribunal</span></a></p>
    <p class="subsection"><span lang="EN-AU"> There is hereby established an Administrative Appeals Tribunal.</span></p>
    <p class="ActHead5"><a name="_Toc514678026"><span class="CharSectno"><span lang="EN-AU">5A</span></span><span
                lang="EN-AU"> Membership</span></a></p>
    <p class="subsection"><span lang="EN-AU"> The Tribunal consists of the following members:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) the President;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) Deputy Presidents;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) senior members;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) other members.</span></p>
    <p class="ActHead3" style="page-break-before:always"><a name="_Toc514678027"><span class="CharDivNo"><span
                    lang="EN-AU">Division2</span></span><span lang="EN-AU">—<span class="CharDivText">Members of
                    Tribunal</span></span></a></p>
    <p class="ActHead5"><a name="_Toc514678028"><span class="CharSectno"><span lang="EN-AU">6</span></span><span
                lang="EN-AU"> Appointment of members of Tribunal</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) The members shall be appointed by the Governor‑General.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) A Judge who is to be appointed as a member of the Tribunal is to be
            appointed as the President or a Deputy President.</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) A person (other than a Judge) who is to be appointed as a member of
            the Tribunal is to be appointed as:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) a Deputy President; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) a senior member (level 1); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) a senior member (level 2); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) a member (level 1); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) a member (level 2); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (f) a member (level 3).</span></p>
    <p class="subsection"><span lang="EN-AU"> (4) A member (other than a Judge) shall be appointed either as a full‑time
            member or as a part‑time member.</span></p>
    <p class="ActHead5"><a name="_Toc514678029"><span class="CharSectno"><span lang="EN-AU">7</span></span><span
                lang="EN-AU"> Qualifications for appointment</span></a></p>
    <p class="SubsectionHead"><span lang="EN-AU">President</span></p>
    <p class="subsection"><span lang="EN-AU"> (1) A person must not be appointed as the President unless the person is a
            Judge of the Federal Court of Australia.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Deputy President</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) A person must not be appointed as a Deputy President unless the
            person:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) is a Judge of the Federal Court of Australia or the Family Court of
            Australia; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) is enrolled as a legal practitioner (however described) of the High
            Court or the Supreme Court of a State or Territory and has been so enrolled for at least 5 years; or</span>
    </p>
    <p class="paragraph"><span lang="EN-AU"> (c) in the opinion of the Governor‑General, has special knowledge or skills
            relevant to the duties of a Deputy President.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Senior members and other members</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) A person must not be appointed as a senior member or other member
            unless the person:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) is enrolled as a legal practitioner (however described) of the High
            Court or the Supreme Court of a State or Territory and has been so enrolled for at least 5 years; or</span>
    </p>
    <p class="paragraph"><span lang="EN-AU"> (b) in the opinion of the Governor‑General, has special knowledge or skills
            relevant to the duties of a senior member or member.</span></p>
    <p class="ActHead5"><a name="_Toc514678030"><span class="CharSectno"><span lang="EN-AU">7A</span></span><span
                lang="EN-AU"> Appointment of a Judge as a presidential member not to affect tenure etc.</span></a></p>
    <p class="subsection"><span lang="EN-AU"> The appointment of a Judge as a presidential member, or service by a Judge
            as a presidential member, whether the appointment was or is made or the service occurred or occurs before or
            after the commencement of this section, does not affect, and shall be deemed never to have affected, his or
            her tenure of office as a Judge or his or her rank, title, status, precedence, salary, annual or other
            allowances or other rights or privileges as the holder of his or her office as a Judge and, for all
            purposes, his or her service, whether before or after the commencement of this section, as a presidential
            member shall be taken to have been, or to be, service as the holder of his or her office as a Judge.</span>
    </p>
    <p class="ActHead5"><a name="_Toc514678031"><span class="CharSectno"><span lang="EN-AU">8</span></span><span
                lang="EN-AU"> Term of appointment</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (3) Subject to this Part, a member holds office for such period of at most
            7 years as is specified in the instrument of appointment, but is eligible for re‑appointment.</span></p>
    <p class="subsection"><span lang="EN-AU"> (4) A member who is a Judge ceases to hold office as a member if he or she
            ceases to be a Judge.</span></p>
    <p class="subsection"><span lang="EN-AU"> (7) Subject to this Part, a member holds office on such terms and
            conditions as are determined by the Minister in writing.</span></p>
    <p class="ActHead5"><a name="_Toc514678032"><span class="CharSectno"><span lang="EN-AU">9</span></span><span
                lang="EN-AU"> Remuneration and allowances</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) A member, other than a member who is a Judge, shall be paid such
            remuneration as is determined by the Remuneration Tribunal, but, if no determination of that remuneration by
            the Remuneration Tribunal is in operation, he or she shall be paid such remuneration as is
            prescribed.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) A member to whom subsection(1) applies shall be paid such allowances
            as are prescribed.</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) Subsections(1) and (2) have effect subject to the <i>Remuneration
                Tribunal Act 1973</i>.</span></p>
    <p class="ActHead5"><a name="_Toc514678033"><span class="CharSectno"><span lang="EN-AU">10</span></span><span
                lang="EN-AU"> Acting appointments</span></a></p>
    <p class="SubsectionHead"><span lang="EN-AU">Acting President</span></p>
    <p class="subsection"><span lang="EN-AU"> (1) The Minister may, by written instrument, appoint a Judge of the
            Federal Court of Australia to act as the President:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) during a vacancy in the office of President (whether or not an
            appointment has previously been made to the office); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) during any period, or during all periods, when the President:</span>
    </p>
    <p class="paragraphsub"><span lang="EN-AU"> (i) is absent from duty or from Australia; or</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (ii) is, for any reason, unable to perform the duties of office.</span>
    </p>
    <p class="notetext"><span lang="EN-AU">Note: For rules that apply to acting appointments, see sections33AB and 33A
            of the <i>Acts Interpretation Act 1901</i>.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Acting member (other than President)</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) The Minister may, by written instrument, appoint a person to act as a
            member (other than the President) during any period, or during all periods, when:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) a full‑time member is absent from duty or from Australia; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) a part‑time member is unavailable to perform the duties of
            office.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Qualification for acting appointment</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) A person must not be appointed to act in an office under subsection(1)
            or (2) unless the person meets the requirements in section7 for appointment to the office.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Extension of acting appointment</span></p>
    <p class="subsection"><span lang="EN-AU"> (5) Where a person has been appointed under subsection(2), the Minister
            may, by reason of a pending proceeding or other special circumstances, direct, before the absent or
            unavailable member ceases to be absent or unavailable, that the person so appointed shall continue to act
            under the appointment after the member ceases to be absent or unavailable until he or she resigns the
            appointment or the Governor‑General terminates the appointment, but a person shall not continue to act by
            virtue of this subsection for more than 12 months after the member ceases to be absent or
            unavailable.</span></p>
    <p class="subsection"><span lang="EN-AU"> (6) Where a person has been appointed under this section to act as a
            member during the absence or unavailability of a member and the member ceases to hold office without having
            resumed duty or become available to perform the duties of his or her office, the period of appointment of
            the person so appointed shall, subject to this Act, be deemed to continue until he or she resigns the
            appointment, the appointment is terminated by the Governor‑General or a period of 12 months elapses from the
            day on which the absent or unavailable member ceases to hold office, whichever first happens.</span></p>
    <p class="SubsectionHead"><span lang="EN-AU">Terms and conditions of acting appointment</span></p>
    <p class="subsection"><span lang="EN-AU"> (7) Subject to this Part, a person (other than a Judge) appointed to act
            in an office under subsection(2) is to act on such terms and conditions as the Minister determines in
            writing.</span></p>
    <p class="ActHead5"><a name="_Toc514678034"><span class="CharSectno"><span lang="EN-AU">10A</span></span><span
                lang="EN-AU"> Delegation</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) The Minister may, by signed instrument, delegate to the President any
            or all of the Minister’s powers or functions under this Act.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) The President may, by signed instrument, delegate to a member any or
            all of the President’s powers or functions under this Act or another enactment.</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) The Registrar may, by signed instrument, delegate to an officer of the
            Tribunal or a member of the staff of the Tribunal any or all of the Registrar’s powers or functions under
            this Act or another enactment.</span></p>
    <p class="subsection"><span lang="EN-AU"> (4) In exercising powers or performing functions under a delegation, the
            delegate must comply with any directions of the delegator.</span></p>
    <p class="ActHead5"><a name="_Toc514678035"><span class="CharSectno"><span lang="EN-AU">10B</span></span><span
                lang="EN-AU"> Oath or affirmation of office</span></a></p>
    <p class="subsection"><span lang="EN-AU"> A person who is appointed or re‑appointed after the commencement of this
            section as a member shall, before proceeding to discharge the duties of his or her office, take before the
            Governor‑General, a Justice of the High Court or a Judge of another federal court or of the Supreme Court of
            a State or Territory an oath or affirmation in accordance with the form in Schedule2.</span></p>
    <p class="ActHead5"><a name="_Toc514678036"><span class="CharSectno"><span lang="EN-AU">11</span></span><span
                lang="EN-AU"> Outside employment</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) A full‑time member must not engage in paid employment outside the
            duties of his or her office without the President’s approval.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) A part‑time member must not engage in any paid employment that, in the
            President’s opinion, conflicts or may conflict with the proper performance of his or her duties.</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) This section does not apply in relation to the holding by a member of
            an office or appointment in the Defence Force.</span></p>
    <p class="ActHead5"><a name="_Toc514678037"><span class="CharSectno"><span lang="EN-AU">12</span></span><span
                lang="EN-AU"> Leave of absence</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) A full‑time member has the recreation leave entitlements that are
            determined by the Remuneration Tribunal.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) The President may grant a full‑time member leave of absence, other
            than recreation leave, on the terms and conditions as to remuneration or otherwise that the Minister
            determines.</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) The President may grant leave of absence to a part‑time member on the
            terms and conditions that the President determines.</span></p>
    <p class="ActHead5"><a name="_Toc514678038"><span class="CharSectno"><span lang="EN-AU">13</span></span><span
                lang="EN-AU"> Termination of appointment (not Judges)</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) The Governor‑General may terminate the appointment of a member if an
            address praying for the termination, on one of the following grounds, is presented to the Governor‑General
            by each House of the Parliament in the same session:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) proved misbehaviour;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) the member is unable to perform the duties of his or her office because
            of physical or mental incapacity.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) The Governor‑General may terminate the appointment of a member
            if:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) the member:</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (i) becomes bankrupt; or</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (ii) takes steps to take the benefit of any law for the relief of
            bankrupt or insolvent debts; or</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (iii) compounds with one or more of his or her creditors; or</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (iv) makes an assignment of his or her remuneration for the benefit of
            one or more of his or her creditors; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) the member is a full‑time member and is absent, except on leave of
            absence, for 14 consecutive days or for 28 days in any 12 months; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) the member is a part‑time member and is unavailable, except on leave of
            absence, to perform the duties of his or her office for more than 3 months; or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) the member contravenes section11 (outside employment); or</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) the member fails, without reasonable excuse, to comply with section14
            (disclosure of interests).</span></p>
    <p class="subsection"><span lang="EN-AU"> (3) The Governor‑General may terminate the appointment of a member
            assigned to the Migration and Refugee Division if the member has a direct or indirect pecuniary interest in
            an immigration advisory service.</span></p>
    <p class="subsection"><span lang="EN-AU"> (4) The appointment of a member may not be terminated other than in
            accordance with this section.</span></p>
    <p class="subsection"><span lang="EN-AU"> (5) This section does not apply in relation to a member who is a
            Judge.</span></p>
    <p class="ActHead5"><a name="_Toc514678039"><span class="CharSectno"><span lang="EN-AU">14</span></span><span
                lang="EN-AU"> Disclosure of interests by members</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) If a member who is, or is to be, a member of the Tribunal as
            constituted for the purposes of a proceeding before the Tribunal has a conflict of interest in relation to
            the proceeding, the member:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) must disclose the matters giving rise to that conflict:</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (i) to the parties; and</span></p>
    <p class="paragraphsub"><span lang="EN-AU"> (ii) to the President (or, if the member is the President, the
            Minister); and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) must not take part in the proceeding or exercise any powers in relation
            to the proceeding unless the parties and the President (or, if the member is the President, the Minister)
            consent.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) For the purposes of this section, a member has a conflict of interest
            in relation to a proceeding before the Tribunal if the member has any interest, pecuniary or otherwise, that
            could conflict with the proper performance of the member’s functions in relation to the proceeding.</span>
    </p>
    <p class="subsection"><span lang="EN-AU"> (3) If the President becomes aware that a member who is, or is to be, a
            member of the Tribunal as constituted for the purposes of a proceeding before the Tribunal has a conflict of
            interest in relation to the proceeding, the President:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) may, if the President considers it appropriate, direct the member not
            to take part in the proceeding; and</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) if the President does not give such a direction—must ensure that the
            member discloses the matters giving rise to the conflict to the parties.</span></p>
    <p class="ActHead5"><a name="_Toc514678040"><span class="CharSectno"><span lang="EN-AU">15</span></span><span
                lang="EN-AU"> Resignation</span></a></p>
    <p class="subsection"><span lang="EN-AU"> (1) A member may resign his or her appointment by giving the
            Governor‑General a written resignation.</span></p>
    <p class="subsection"><span lang="EN-AU"> (2) The resignation takes effect on the day it is received by the
            Governor‑General or, if a later day is specified in the resignation, on that later day.</span></p>
    <p class="ActHead2" style="page-break-before:always"><a name="_Toc514678041"><span class="CharPartNo"><span
                    lang="EN-AU">PartIII</span></span><span lang="EN-AU">—<span class="CharPartText">Organisation of the
                    Tribunal</span></span></a></p>
    <p class="ActHead3"><a name="_Toc514678042"><span class="CharDivNo"><span lang="EN-AU">Division1</span></span><span
                lang="EN-AU">—<span class="CharDivText">Divisions of the Tribunal</span></span></a></p>
    <p class="ActHead4"><a name="_Toc514678043"><span class="CharSubdNo"><span lang="EN-AU">Subdivision
                    A</span></span><span lang="EN-AU">—<span class="CharSubdText">Divisions of the
                    Tribunal</span></span></a></p>
    <p class="ActHead5"><a name="_Toc514678044"><span class="CharSectno"><span lang="EN-AU">17A</span></span><span
                lang="EN-AU"> Divisions of the Tribunal</span></a></p>
    <p class="subsection"><span lang="EN-AU"> The Tribunal is to exercise powers conferred on it in the following
            Divisions:</span></p>
    <p class="paragraph"><span lang="EN-AU"> (aa) Freedom of Information Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (a) General Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (b) Migration and Refugee Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (c) National Disability Insurance Scheme Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (d) Security Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (e) Social Services and Child Support Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (f) Taxation and Commercial Division;</span></p>
    <p class="paragraph"><span lang="EN-AU"> (g) any other prescribed Division.</span></p>
    <p class="ActHead5"><a name="_Toc514678045"><span class="CharSectno"><span lang="EN-AU">17B</span></span><span
                lang="EN-AU"> Allocation of business to Divisions</span></a></p>
</div>
```

We are going to create a source tranformer for it and practically, ~~fingers crossed~~, any ACT html document from the site.

### Create the source transformation file and js/test file

Navigate to the `Sources` folder and create `AustralianFederal.ts` file. The file should implement the `OptimusTransformer` interface. Just copy over the `Template.ts` file contents and edit accordingly. Minimum file contents should look like this:

`src/Transformer/OptimusTransformer/Source/AustraliaFederal.ts`
```JavaScript
import OptimusTransformer from '../OptimusTransformer';
import {LevelIdentifier} from '../Interfaces/LevelIdentifier';
import TextClassification from '../Modules/TextClassification';

export class AustraliaFederal extends OptimusTransformer {
    // Class for transforming legal documents from the Australia Federal source
    constructor(htmlString: string, indentation: number = 30) {
        super(htmlString, indentation);
        this.definitionRegex = this.definitionRegexArray;
        this.textClassifier = new TextClassification(
        this.levelIdentification,
        this.hcontainers,
        this.indentation,
        this.definitionRegex
        );
    }

    // RegExp string for matching the note identifier
    // Needs to be an html attribute on the note container element - ideally a div
    protected notesIdentifier: string | null;

    // The human-readable name of the source
    public static sourceName: string = 'Australian Federal Law';

    // Internal Transformer Reference
    public static transformerName: string = 'Australian Federal Law Transformer';

    // An array of levels that appear in the document,
    // in the order that they could appear in,
    // and containing regex to match the number of that level
    protected levelIdentification: Array<LevelIdentifier> = [];

    // List of non-standard hcontainers that exist within the document
    protected hcontainers: Array<LevelIdentifier> = [];

    // The TextClassification instance
    protected textClassifier: TextClassification;

    // Here you may define an array of RegExpressions to match definitions
    private definitionRegexArray: RegExp[] = [];

    // RegExp string for matching the table identifier
    // Needs to be an html attribute on the table element
    protected tableIdentifier: string | null = null;

    // Pipeline function (See: TransformerPipeline.ts)
    // Source specific transform to prepare text for transformation
    protected sourceTextTransform(html: string): string {
        return html;
    }

    // Pipeline function (See: TransformerPipeline.ts)
    // Preprocess function to be applied to html string
    protected preProcess(html: string): string {
        return html;
    }

    // Pipeline function (See: TransformerPipeline.ts)
    // Postprocess function to be applied to html string
    protected postProcess(html: string): string {
        return html;
    }
}
```

Next, we add this new source into the list of the already existing Transformers. This allows us to call it in our `js/test` file. Navigate to `src/Transformer/index.ts` and update it to alsoexport `AustraliaFederal` transformer. i.e.

```JavaScript
// code omitted for brevity
import {
  AustraliaFederal
} from './OptimusTransformer/Sources/AustraliaFederal';

// code omitted for brevity
export {AustraliaFederal}
```

I will use `transform.js` file name in this tutorial for my `js` file.

a) Open a new terminal in the project root and:

1. Install the dependencies i.e. `yarn install`.
2. Run the watch command i.e. `yarn run watch`.

b) Open another terminal in the root of the project and:

1. Create a `transform.js` file.
2. Insert this minimal code and run `node transform.js`.

`transform.js`
```JavaScript
const Transformer = require('./build/src/Transformer');
const fs = require('fs');
const { promisify } = require('util');
const readFileAsync = promisify(fs.readFile);
const { DocumentParser } = require('./build/src/Parser');
const { Engine } = require('nkyimu');

const { JSDOM } = require('jsdom');

global.XMLSerializer = class XMLSerializer {
  serializeToString(dom) {
    return require('xmlserializer').serializeToString(dom);
  }
};
const dom = new JSDOM();

global.Document = dom.window.Document;
global.Element = dom.window.Element;
global.document = dom.window.document;
global.Text = dom.window.Text;
global.Comment = dom.window.Comment;
global.DOMParser = dom.window.DOMParser;
global.Node = dom.window.Node;

async function run() {
    // Reading the file from disk into a string
    const auFederalHtml = await readFileAsync('au-act.html');
    const transformer = new Transformer.AustraliaFederal(auFederalHtml);
    let legalDocHTMLString = await transformer.toLegalDocHTMLString();
    return legalDocHTMLString;
}
run()
  .then(result => {
    console.log(result);
  })
  .catch(err => {
    console.error(err);
  });
```

This code reads an HTML file contents and converts it into a string, which is then passed into the transformer and if it's successful, it logs out the legal doc html string.

> **NOTE:** In the step (b) above, if you're using a `.test.ts` file, you can have minimal code like:

`__tests__/australia-federal.test.ts`
```JavaScript
import { AustraliaFederal } from '../src/Transformer';
import { DocumentParser } from '../src/Parser/DocumentParser';

class XMLSerializer {
  serializeToString(dom) {
    return require('xmlserializer').serializeToString(dom);
  }
}

beforeAll(() => {
  window['XMLSerializer'] = XMLSerializer;
});

describe('Australia Federal Law Transformation', async () => {
  let parser: DocumentParser;

  beforeEach(() => {
  });

  it('Should return a valid legal doc html string', async () => {
    const transformer = new AustraliaFederal(html); // read html from disk or copy a string
    const legalDocHtmlString = await transformer.toLegalDocHTMLString();
    console.log(legalDocHtmlString); // log the output
    expect(legalDocHtmlString).toContain(`id="notes"`);
    expect(legalDocHtmlString).toContain(`id="preamble"`);
    expect(legalDocHtmlString).toContain(`id="body"`);
  });
});
```

Going ahead, we're going to update parts of the `AustraliaFederal.ts` file. If you run the transform above with node, the following output should appear on your console.

```html
<div id="notes"></div>
<div id="preamble"></div>
<div id="body">
    <p>An Act to establish an Administrative Appeals Tribunal</p>
    <p>PartI</p>
    <p>—</p>
    <p>Preliminary</p>
    <p></p>
    <p>1</p>
    <p>Short title</p>
    <p>This Act may be cited as the Administrative Appeals Tribunal Act 1975.</p>
    <p>2</p>
    <p>Commencement</p>
    ...
</div>
```

The notes and preamble sections are empty. As for the body, we are not sure what the numbers mean, how to tell the parts or sections etc. In the next steps, we are going to walk through how you can improve the transformer until you have a legal doc that matches the html.

### Testing and debugging a Transformer

#### Generation of the notes

##### Using an existing identifier

In the html above, notes appear like this:

```html
// code omitted for brevity
<p class="notetext"><span lang="EN-AU">Note: See also subsection25(8) (Norfolk Island enactments).</span></p>
// code omitted for brevity
```

This is easy! It has an identifying `class="notetext"` which we can use to get the notes. Update your `AustraliaFederal.ts` file like this:

```javascript
protected notesIdentifier = `class="notetext"`;
```

When you run the `transformation.js` code, you will get an output like this:

```html
<div id="notes">
    <note eid="note_seq0">
        <p>Note: See also subsection25(8) (Norfolk Island enactments).</p>
    </note>
    <note eid="note_seq1">
        <p>Note: For rules that apply to acting appointments, see sections33AB and 33A of the Acts Interpretation Act 1901.</p>
    </note>
</div>
<div id="preamble"></div>
<div id="body">
    <p>An Act to establish an Administrative Appeals Tribunal</p>
    <p>PartI</p>
    <p>—</p>
    <p>Preliminary</p>
    ...
</div>
```

The notes are updated. You should also notice that in the `body` div, the location where this notes were have been replaced with a `noteRef` node and a digit.

```
...
<span data-inline="noteRef" data-ref="#note_seq0">0</span>
...
```

Basically, that's all you can do for this document. Now, suppose there was no class marker and the the only way we could tell that it's a note is by looking at the text, how could we go about that? We could pre-process the html and create an identifier of our own.

##### 2. Creating your own identifier

There are 3 import methods that `OptimusTransformer` implements which we can use to process html and legal doc strings. They accept an HTML string input, and return a transformed string as an output.

### HTML Pre-Processor (`preProcess`)

This is the very first transformation that is applied to the string. It can be used to add necessary attributes required to correctly extract tables if that attribute is not present.

### Source Specific Transform (`sourceTextTransform`)

This transform applies transformations that are specific to the source text that are required to get the text ready to be converted to `legalDocHTML`. For example, a source may have the headings come before the number, however `legalDocHTML` requires the numbers to come before the headings, and for them to be on the same line. So we can use the `sourceTextTransform` function to format our text appropriately.

Example:

```javascript
protected sourceTextTransform(text: string): string {
  return text
    .replace(
    /((?:part|chapter)\s[a-z0-9]+\.?)<\/p><p>((?!<\/p).+\b<\/p)/gim,
    '$1 $2'
    )
    .replace(/([^<>]+<\/p>)(<p>)([0-9]+\.)/gim, '$3 $1$2')
    .replace(/(<p>)—+/gm, '$1')
    .replace(/(<p>SCHEDULE)<\/p><p>([^<]+)/g, '$1 $2')
    .replace(/(<p>\([0-9]+\))\s+(\([a-z]+\))/g, '$1</p><p>$2');
}
```

This function runs after the html has been cleaned, flattened and sanitized.

### HTML Post-Processor (`postProcess`)

Similar to the `preProcess` function, this function runs as the very last transformation and can be used for any cleaned required.

Going back to our scenario, suppose the notes for this document were like this:

```html
<p><span lang="EN-AU">Note: See also subsection25(8) (Norfolk Island enactments).</span></p>
```

We can see that a note text begins with the text "Note:". We know that `preProcess` works on the html document before it's flattened out. We can write a string replace statement and use regex to replace all the text after "Note:" into a new `<p>` tag with a `class="notetext"`. We will leave the `notesIdentifier` variable as is.

```javascript
protected preProcess(html: string): string {
    html = html
      .toString()
      // TIP: replace more than 2 white spaces with 2. This ensures that
      // the regex you run works on a string without weird line breaks
      .replace(/\s{2,}/gm, ' ')

      // Get the text after 'Note:' and wrap it into <p> tags with the
      // identifier then replace the entire <span>...</span> tag with it

      .replace(/<span\s+.*?>Note:\s+(.*?)<\/span>/gim, `<p class="notetext">$1</p>`);
      //                             ^^^
      // we're extracting this text here using $1. More on regex here: http://bit.ly/2ZvdsTv
    return html;
}
```

After updating, rerun and you will get output as above only that it'll exclude the word "Note:". *TIP:* You can update your regex to include the words if you want.

Another thing we can do with the functions is change the legal doc html output. This can be done in the `postProcess` function. In our notes, we get the `eid="note_seq0"` attribute name. This will fail transformation to AkN since the attribute name `eid` is not a valid one! ~~BUG alert~~. The lowercase `i` needs to capitalized. We can fix this in `postProcess` by replacing all occurences of lower-case `i` `eid` with a valid attribute name`eId`.

```javascript
protected postProcess(html: string): string {
    const noteEidAttribute = /eid=/gim;
    html = html.replace(noteEidAttribute, 'eId=');
    return html;
}
```

That's all for notes!

#### Generation of the preamble

Well, the section of code we have is not clear as to what can be used for the `preamble` of the code. We are basically looking for introductory text, titles and since this is just a section of an entire document, we're limited. So the next parts are in reference to the entire document. You can copy the `innerHTML` of the document on this [link](https://www.legislation.gov.au/Details/C2018C00172) and paste it into your file or your html string.

##### Identifying a preamble block

The same way we identified notes earlier, you can be lucky to get a document source that has clearly indicated what section of the document is a preamble. Assuming we did and we have an html string like this:

```html
...
<div class="preamble">
    <p>Long title text</p>
    <p>Compilation text and other information</p>
    ...
</div>
```

Then, we can easily write a replace statement that captures this div and puts it into the required `<preamble>...</preamble>` tag. This will get you the entire text in the `<div>` into the preamble section of the legal doc html.

For our document, we see that the `<div class="WordSection1">...</div>` contains most of the introductory text of the act. We can use the replace statement strategy above and endup with something like this:

```javascript
protected preProcess(html: string): string {
    // code ommitted
    .replace(/\s{2,}/gm, ' ')
    .replace(/(<div class="WordSection1">.*?<\/div>)/gim, `<preamble>$1</preamble>`)

    return html;
}
```

This will produce the following content in the `preamble` section:

```html
<div id="preamble">
    <p>Administrative Appeals Tribunal Act 1975</p>
    <p>No.&nbsp;91, 1975</p>
    <p>Compilation No.</p>
    <p>46</p>
    <p>Compilation date:</p>
    <p>11 May 2018</p>
    <p>Includes amendments up to:</p>
    <p>Act No. 31, 2018</p>
    <p>Registered:</p>
    <p>22 May 2018</p>
    <p>About this compilation</p>
    <p>This compilation</p>
    <p>This is a compilation of the</p>
    <p>Administrative Appeals Tribunal Act 1975</p>
    <p>that shows the text of the law as amended and in force on</p>
    <p>11 May 2018</p>
    <p>(the <i>compilation date).</i></p>
    <p><i>The notes at the end of this compilation (the <i>endnotes) include information about amending laws and the amendment history of provisions of the compiled law.</i></i>
    </p>
    <p><i><i>Uncommenced amendments</i></i>
    </p>
    <p><i><i>The effect of uncommenced amendments is not shown in the text of the compiled law. Any uncommenced amendments affecting the law are accessible on the Legislation Register (www.legislation.gov.au). The details of amendments made up to, but not commenced at, the compilation date are underlined in the endnotes. For more information on any uncommenced amendments, see the series page on the Legislation Register for the compiled law.</i></i>
    </p>
    <p><i><i>Application, saving and transitional provisions for provisions and amendments</i></i>
    </p>
  ....
</div>
```

Generally, the idea here is to figure out the preamble section and wrap it into `<preamble>..</preamble>` tags. Easy peasy!

